name: open_centrifugal_fan_database
version_number: 1.0.0
institution: Karlsruhe Institute of Technology
contact: matthias.probst@kit.edu
valid_characters: ''
pattern: ''
last_modified: 2023-03-13_09:58:22
---
table:
  absolute_air_pressure:
    canonical_units: Pa
    description: Pressure is force per unit area. Absolute air pressure is pressure
      deviation to a total vacuum.
  air_density:
    canonical_units: kg/m**3
    description: Air density is defined as the mass of air divided by its volume.
  air_mass_flow_rate:
    canonical_units: kg/s
    description: Air mass flow rate is the mass of air that passes a certain cross
      sectiont per unit time.
  air_power:
    canonical_units: W
    description: Power of moving fluid.
  air_temperature:
    canonical_units: degC
    description: Air temperature is the bulk temperature of the air, not the surface
      (skin) temperature. (CF Conventions)
  air_volume_flow_rate_at_fan_outlet:
    canonical_units: m**3/s
    description: Air volume flow rate is the volume of air that passes a certain cross
      section per unit time. "at_fan_inlet" indicates that this measure applies to
      the fan inlet surface.
  air_volume_flow_rate_of_fan:
    canonical_units: m**3/s
    description: Air volume flow rate is the volume of air that passes a cross section
      per unit time.
  ambient_air_temperature:
    canonical_units: K
    description: Air temperature is the bulk temperature of the air, not the surface
      (skin) temperature. Ambient air temperature is the temperature of the surrounding
      air.
  ambient_static_air_pressure:
    canonical_units: Pa
    description: Static air pressure is the amount of pressure exerted by air that
      is not moving. Ambient static air pressure is the static air pressure of the
      surrounding air.
  blade_angle_of_fan:
    canonical_units: rad
    description: Angle of blade shape typically at inlet or outlet.
  difference_of_dynamic_air_pressure:
    canonical_units: Pa
    description: Dynamic air pressure is a measure for kinetic energy per unit volume
      of moving air. Difference of dynamic air pressure is the difference between
      dynamic air pressures of two points.
  difference_of_static_air_pressure:
    canonical_units: Pa
    description: Static air pressure is the amount of pressure exerted by air that
      is not moving. Difference of static air pressure is the difference between static
      air pressures of two points.
  difference_of_static_air_pressure_across_orifice:
    canonical_units: Pa
    description: Differnece of static air pressure across orifice to compute volume
      flow rate according to DIN EN ISO 5167.
  difference_of_total_air_pressure:
    canonical_units: Pa
    description: Total air pressure is the sum of static and dynamic air pressure.
      Difference of total air pressure is the difference between total air pressures
      of two points.
  difference_of_wall_static_air_pressure_across_fan:
    canonical_units: Pa
    description: Static air pressure is the amount of pressure exerted by air that
      is not moving. Difference of wall static air pressure across a fan is the difference
      between the static air pressure downstream of the fan and the total air pressure
      upstream of the fan at the wall.
  difference_of_wall_static_air_pressure_across_orifice:
    canonical_units: Pa
    description: Static air pressure is the amount of pressure exerted by air that
      is not moving. Differnece of wall static air pressure across orifice is the
      difference between the static air pressure downstream of the orifice and the
      static air pressure upstream of the orifice at the wall.
  dynamic_air_pressure:
    canonical_units: Pa
    description: Dynamic air pressure is a measure for kinetic energy per unit volume
      of moving air.
  dynamic_air_viscosity:
    canonical_units: Pa*s
    description: Dynamic air viscosity indicates the resistance  of air towards deformation
      under shear stress. (https://doi.org/10.1016/B978-0-08-096949-7.00020-0)
  exposure:
    canonical_units: s
    description: Duration that camera sensor is exposed to light.
  fan_impeller_inner_diameter:
    canonical_units: m
    description: The inner diameter of deployed fan.
  fan_impeller_outlet_diameter:
    canonical_units: m
    description: The outer diameter of deployed fan.
  fan_inlet_cross_sectional_area:
    canonical_units: m
    description: The fan cross-sectional area at the inlet section for fans with a
      casing. The position of the referred cross-sectional area is in the pipe upstream
      of the fan. The area is generally taken to compute the dynamic pressure at the
      inlet of the fan based on the volume flow rate.
  fan_outlet_cross_sectional_area:
    canonical_units: m
    description: The fan cross-sectional area at the outlet section for fans with
      a casing. The position of the referred cross-sectional area is in the pipe downstream
      of the fan. The area is generally taken to compute the dynamic pressure at the
      outlet of the fan based on the volume flow rate.
  fan_rotational_speed:
    canonical_units: 1/s
    description: Number of revolutions of a fan.
  fan_torque:
    canonical_units: Nm
    description: The torque acting on the impeller of the fan.
  final_interrogation_window_size:
    canonical_units: pixel
    description: Size of the final interogation window.
  flow_coefficient_of_fan:
    canonical_units: ''
    description: Air flow coefficient is a dimensionless number as defined in (CAROLUS,
      Thomas. Ventilatoren-Aerodynamischer Entwurf, Schallvorhersage. Konstruktion,
      2013, 2. Jg., p.2, eq.1.3). The addition "of_fan" indicates that this coefficient
      applies to the deployed fan.
  inner_diameter_of_orifice:
    canonical_units: m
    description: Inner diameter of an orifice.
  kinematic_air_viscosity:
    canonical_units: m**2/s
    description: Dynamic air viscosity indicates the resistance  of air towards deformation
      under shear stress. Kinematic viscosity. Dynamic air viscosity divided by air
      denisity equals kinematic air viscosity. (https://doi.org/10.1016/B978-0-12-410461-7.00007-9)
  magnitude_of_air_velocity:
    canonical_units: m/s
    description: Magnitude of the vector quantity velocity.
  overlap:
    canonical_units: pixel
    description: Overlap of interogation windows.
  piv_correlation_coefficient:
    canonical_units: ''
    description: Maximum correlation coefficient of PIV vector. Further information
      may be provided by the used PIV software.
  piv_flag:
    canonical_units: ''
    description: PIV flag is defined by the used PIV evaluation software and generally
      indicates the validity of the PIV vector.
  piv_scaling_factor:
    canonical_units: pixel/m
    description: Scaling factor between image and object plane.
  power_of_driveshaft:
    canonical_units: W
    description: Power of driveshaft.
  pulse_delay:
    canonical_units: s
    description: Time between two laser pulses.
  relative_humidity:
    canonical_units: ''
    description: Relative humidity is a measure of the water vapor content of air.
  rotational_speed_of_auxiliary_fan:
    canonical_units: 1/s
    description: Number of revolutions of an auxiliary fan.
  rotor_diameter_of_fan:
    canonical_units: m
    description: Diameter of an impeller rotor.
  signal_to_noise:
    canonical_units: ''
    description: Signal to noise ratio as defined in (ADRIAN, Ronald J.; WESTERWEEL,
      Jerry. Particle image velocimetry. Cambridge university press, 2011. p.152,
      eq.4.17)
  sound_pressure_in_air:
    canonical_units: Pa
    description: Sound pressure is the pressure deflection from the ambient pressure
      induced by a sound. (978-0-7506-7291-7)
  static_air_pressure:
    canonical_units: Pa
    description: Static air pressure is the amount of pressure exerted by air that
      is not moving.
  time:
    canonical_units: s
    description: Recording time since start of experiment.
  torque_of_driveshaft:
    canonical_units: Nm
    description: Torque is a force that causes something to rotate. Torque of driveshaft
      is the torque acting on the driveshaft.
  total_air_pressure:
    canonical_units: Pa
    description: The sum of dynamic and static air pressure.
  total_efficiency_of_fan:
    canonical_units: ''
    description: Total fan efficiency as defined in (CAROLUS, Thomas. Ventilatoren-Aerodynamischer
      Entwurf, Schallvorhersage. Konstruktion, 2013, 2. Jg., p.5, eq.1.16).
  total_pressure_coefficient_of_fan:
    canonical_units: ''
    description: Total pressure coefficient is a dimensionless number as defined in
      (CAROLUS, Thomas. Ventilatoren-Aerodynamischer Entwurf, Schallvorhersage. Konstruktion,
      2013, 2. Jg., p.2, eq.1.4).  The addition "of_fan" indicates that this coefficient
      applies for the deployed fan.
  total_to_static_air_pressure_across_fan:
    canonical_units: Pa
    description: The difference of static pressure at fan outlet w.r.t. the total
      pressure upstream of the fan. The total pressure generally is not known at the
      fan inlet pipe but further upstream, e.g. in a settling chamber. The dataset
      must provide detailed information, e.g. referencing to the respective pressure
      measurement device containing the exact location in the setup.
  turbulent_kinetic_energy:
    canonical_units: m**2/s**2
    description: The kinetic energy per unit mass of a fluid.
  weight_of_impeller:
    canonical_units: kg
    description: Weight of the impeller.
  x_air_velocity:
    canonical_units: m/s
    description: Velocity is a vector quantity. X indicates the component in x-axis
      direction.
  x_coordinate:
    canonical_units: m
    description: Coordinate in x axis direction.
  x_derivative_of_x_air_velocity:
    canonical_units: 1/s
    description: Derivative of x air velocity in x-axis direction.
  x_derivative_of_y_air_velocity:
    canonical_units: 1/s
    description: Derivative of y air velocity in x-axis direction.
  x_derivative_of_z_air_velocity:
    canonical_units: 1/s
    description: Derivative of z air velocity in x-axis direction.
  x_displacement_of_peak1:
    canonical_units: pixel
    description: Displacement of strongest correlation peak in x-axis direction.
  x_displacement_of_peak2:
    canonical_units: pixel
    description: Displacement of second strongest correlation peak in x-axis direction.
  x_displacement_of_peak3:
    canonical_units: pixel
    description: Displacement of third correlation peak in x-axis direction.
  x_forces_of_impeller_due_to_friction:
    canonical_units: N
    description: Force is a vector quantity. "x" indicats forces acting along x-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  x_forces_of_impeller_due_to_pressure:
    canonical_units: N
    description: Force is a vector quantity. "x" indicats forces acting along x-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  x_pixel_coordinate:
    canonical_units: pixel
    description: Pixel coordinate in x-axis direction.
  x_torque_of_impeller_due_to_pressure:
    canonical_units: Nm
    description: Torque is a vector quantity. "x" indicats forces acting along x-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  x_torque_ofimpeller_due_to_friction:
    canonical_units: Nm
    description: Torque is a vector quantity. "x" indicats forces acting along x-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  y_air_velocity:
    canonical_units: m/s
    description: Velocity is a vector quantity. Y indicates the component in z-axis
      direction.
  y_coordinate:
    canonical_units: m
    description: Coordinate in y axis direction.
  y_derivative_of_x_air_velocity:
    canonical_units: 1/s
    description: Derivative of  x air velocity in y-axis direction.
  y_derivative_of_y_air_velocity:
    canonical_units: 1/s
    description: Derivative of y air velocity in y-axis direction.
  y_derivative_of_z_air_velocity:
    canonical_units: 1/s
    description: Derivative of z air velocity in y-axis direction.
  y_displacement_of_peak1:
    canonical_units: pixel
    description: Displacement of strongest correlation peak 1 in y-axis direction.
  y_displacement_of_peak2:
    canonical_units: pixel
    description: Displacement of second strongest correlation peak in y-axis direction.
  y_displacement_of_peak3:
    canonical_units: pixel
    description: Displacement of third strongest correlation peak 3 in y-axis direction.
  y_forces_of_impeller_due_to_friction:
    canonical_units: N
    description: Force is a vector quantity. "y" indicats forces acting along y-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  y_forces_of_impeller_due_to_pressure:
    canonical_units: N
    description: Force is a vector quantity. "y" indicats forces acting along y-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  y_pixel_coordinate:
    canonical_units: pixel
    description: Pixel coordinate in y-axis direction.
  y_torque_of_impeller_due_to_friction:
    canonical_units: Nm
    description: Torque is a vector quantity. "y" indicats forces acting along y-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  y_torque_of_impeller_due_to_pressure:
    canonical_units: Nm
    description: Torque is a vector quantity. "y" indicats forces acting along y-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  yx_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "yx" indicates the component in y-axis direction.
  yy_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "yy" indicates the component in y-axis direction.
  yz_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "yz" indicates the component in y-axis direction.
  z_air_velocity:
    canonical_units: m/s
    description: Velocity is a vector quantity. Z indicates the component in y-axis
      direction.
  z_air_vorticity:
    canonical_units: 1/s
    description: Vorticity is a vector quantity. Z indicates the component in z-axis
      direction.
  z_coordinate:
    canonical_units: m
    description: Coordinate in z axis direction.
  z_derivative_of_x_air_velocity:
    canonical_units: 1/s
    description: Derivative of x air velocity in z-axis direction.
  z_derivative_of_y_air_velocity:
    canonical_units: 1/s
    description: Derivative of y air velocity in z-axis direction.
  z_derivative_of_z_air_velocity:
    canonical_units: 1/s
    description: Derivative of z air velocity in z-axis direction.
  z_forces_of_impeller_due_to_friction:
    canonical_units: N
    description: Force is a vector quantity. "z" indicats forces acting along z-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  z_forces_of_impeller_due_to_pressure:
    canonical_units: N
    description: Force is a vector quantity. "z" indicats forces acting along z-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  z_torque_of_impeller_due_to_friction:
    canonical_units: Nm
    description: Torque is a vector quantity. "z" indicats forces acting along z-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_friction"
      isolates term induced by friction.
  z_torque_of_impeller_due_to_pressure:
    canonical_units: Nm
    description: Torque is a vector quantity. "z" indicats forces acting along z-axis
      direction. "of_impeller" indecates that forces are acting on impeller. "due_to_pressure"
      isolates term induced by pressure.
  zx_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "zx" indicates the component in y-axis direction.
  zy_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "zy" indicates the component in y-axis direction.
  zz_air_reynolds_stress:
    canonical_units: m**2/s**2
    description: Reynolds stress is a tensor quantity. "Air" indicates, that the Reynolds
      stress is calculated for air. "zz" indicates the component in y-axis direction.
