import pathlib

import yaml

try:
    from h5rdmtoolbox.conventions.cflike import StandardNameTable
except ImportError:
    raise ImportError('h5rdmtoolbox missing. you need to install it to check for standard name tables')

__this_dir__ = pathlib.Path(__file__).parent


def test_read():
    with open('open_centrifugal_fan_database-v1.yaml', 'r') as f:
        _dict = {}
        for d in yaml.full_load_all(f):
            _dict.update(d)

    snt_local = StandardNameTable.from_yaml(
        __this_dir__ / 'open_centrifugal_fan_database-v1.yaml'
    )
    assert snt_local.versionname == 'open_centrifugal_fan_database-v1'
    snt = StandardNameTable.from_gitlab(url='https://git.scc.kit.edu',
                                        file_path='open_centrifugal_fan_database-v1.yaml',
                                        project_id='35443',
                                        ref_name='main')
    assert snt.versionname == 'open_centrifugal_fan_database-v1'

    snt.sdump()


if __name__ == '__main__':
    test_read()
